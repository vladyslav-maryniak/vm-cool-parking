﻿using System;

namespace CoolParking.BL.Exceptions
{
    internal class FullParkingException : Exception
    {
        public FullParkingException()
        {
        }

        public FullParkingException(string message)
            : base(message)
        {
        }

        public FullParkingException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
