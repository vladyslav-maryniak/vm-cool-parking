﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static readonly Regex validationRegex = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$", RegexOptions.Compiled);
        private static readonly Random generator = new Random();

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = IsValidRegistrationPlateNumber(id) ? id : throw new ArgumentException("Invalid identifier");
            VehicleType = Enum.IsDefined(typeof(VehicleType), vehicleType) ? vehicleType : throw new ArgumentException("Invalid vehicle type");
            Balance = balance > 0 ? balance : throw new ArgumentException("Invalid balance");
        }

        public static bool IsValidRegistrationPlateNumber(string plateNumber) => validationRegex.IsMatch(plateNumber);

        public static string GenerateRandomRegistrationPlateNumber() =>
            $"{GetRandomLetters(2)}-{generator.Next(0, 10000).ToString().PadLeft(4, '0')}-{GetRandomLetters(2)}";

        private static string GetRandomLetters(int length)
        {
            var letters = new char[length];
            for (int i = 0; i < length; i++)
            {
                letters[i] = (char)generator.Next('A', 'Z' + 1);
            }
            return new string(letters);
        }
    }
}