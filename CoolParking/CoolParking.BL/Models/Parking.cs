﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        private static readonly Lazy<Parking> parking = new Lazy<Parking>(() => new Parking());

        public static Parking Instance => parking.Value;
        public decimal Balance { get; set; } = 0.0M;
        public List<Vehicle> Vehicles { get; } = new List<Vehicle>();
        
        private Parking()
        {
        }
    }
}