﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace CoolParking.ClientHttpRequests
{
    class Program
    {
        private const string title = @"
  ________  ________  ________  ___               ________  ________  ________  ___  __    ___  ________   ________     
 |\   ____\|\   __  \|\   __  \|\  \             |\   __  \|\   __  \|\   __  \|\  \|\  \ |\  \|\   ___  \|\   ____\    
 \ \  \___|\ \  \|\  \ \  \|\  \ \  \            \ \  \|\  \ \  \|\  \ \  \|\  \ \  \/  /|\ \  \ \  \\ \  \ \  \___|    
  \ \  \    \ \  \\\  \ \  \\\  \ \  \            \ \   ____\ \   __  \ \   _  _\ \   ___  \ \  \ \  \\ \  \ \  \  ___  
   \ \  \____\ \  \\\  \ \  \\\  \ \  \____        \ \  \___|\ \  \ \  \ \  \\  \\ \  \\ \  \ \  \ \  \\ \  \ \  \|\  \ 
    \ \_______\ \_______\ \_______\ \_______\       \ \__\    \ \__\ \__\ \__\\ _\\ \__\\ \__\ \__\ \__\\ \__\ \_______\
     \|_______|\|_______|\|_______|\|_______|        \|__|     \|__|\|__|\|__|\|__|\|__| \|__|\|__|\|__| \|__|\|_______|
";
        private const string inputTip = "Enter the keyword below to perform the function:";
        private const string inputTitle = "Enter function:";
        private const string endpointException = "Most likely you entered the wrong endpoint. Please enter it again.";
        private delegate void InputStringHandler();
        private static HttpClient client;
        private static Dictionary<string, InputStringHandler> functions;
        private const string defaultHost = "localhost:44308";
        private static string host = defaultHost;

        static void Main(string[] args)
        {
            functions = new Dictionary<string, InputStringHandler>()
            {
                { "balance", DisplayBalance },
                { "capacity", DisplayCapacity },
                { "free places", DisplayFreePlaces },
                { "vehicles", DisplayAllVehicles },
                { "vehicle", DisplayVehicle },
                { "post vehicle", PostVehicle },
                { "delete vehicle", DeleteVehicle },
                { "all transactions", DisplayAllTransactions },
                { "last transactions", DisplayLastTransactions },
                { "top up vehicle", TopUpVehicle },
                { "menu", DisplayMenu },
                { "exit", CloseApp }
            };

            client = new HttpClient();
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chai, sslPolicyErrors) => true;

            DisplayTitle();
            EnterHost();
            Console.WriteLine($"{inputTip}");
            DisplayMenu();

            while (true)
            {
                Console.Write($"{inputTitle} ");
                string input = Console.ReadLine();
                if (string.IsNullOrEmpty(input) || !functions.ContainsKey(input))
                {
                    Console.WriteLine("\nIncorrect input value. Please try again.\n");
                    continue;
                }
                functions[input].Invoke();
            }
        }

        private static void DisplayTitle()
        {
            Console.WriteLine(title);
        }

        private static void EnterHost()
        {
            Console.WriteLine($"\nPlease type the host (Press Enter to assign the default value - {defaultHost}):");
            var input = Console.ReadLine();
            if (string.IsNullOrEmpty(input))
            {
                host = defaultHost;
                return;
            }
            host = input;
            Console.WriteLine();
        }

        private static void DisplayMenu()
        {
            Console.WriteLine();
            foreach (var keyword in functions.Keys)
            {
                Console.WriteLine(keyword.PadLeft(20));
            }
            Console.WriteLine();
        }

        private static void DisplayBalance()
        {
            string response = GetResponse($"https://{host}/api/parking/balance");
            if (string.IsNullOrEmpty(response))
            {
                return;
            }
            Console.WriteLine($"\n\tBalance: {response}\n");
        }

        private static void DisplayCapacity()
        {
            string response = GetResponse($"https://{host}/api/parking/capacity");
            if (string.IsNullOrEmpty(response))
            {
                return;
            }
            Console.WriteLine($"\n\tCapacity: {response}\n");
        }

        private static void DisplayFreePlaces()
        {
            string response = GetResponse($"https://{host}/api/parking/freePlaces");
            if (string.IsNullOrEmpty(response))
            {
                return;
            }
            Console.WriteLine($"\n\tFree places: {response}\n");
        }

        private static void DisplayAllVehicles()
        {
            string response = GetResponse($"https://{host}/api/vehicles");
            if (string.IsNullOrEmpty(response))
            {
                return;
            }

            Console.WriteLine("\n\tParked vehicles:\n");

            foreach (var vehicle in JsonConvert.DeserializeObject<List<Vehicle>>(response))
            {
                Console.WriteLine($"{vehicle}\n");
            }
        }

        private static void DisplayVehicle()
        {
            string id = GetInputVehicleId();
            string response = GetResponse($"https://{host}/api/vehicles/{id}");
            if (string.IsNullOrEmpty(response))
            {
                return;
            }
            var vehicle = JsonConvert.DeserializeObject<Vehicle>(response);
            Console.WriteLine($"\n{vehicle}\n");
        }

        private static void PostVehicle()
        {
            var vehicle = GetInputVehicle();

            var content = new StringContent(JsonConvert.SerializeObject(vehicle), Encoding.UTF8, "application/json");
            var response = PostContent(content);

            if (response != null && response.IsSuccessStatusCode)
            {
                vehicle = JsonConvert.DeserializeObject<Vehicle>(response.Content.ReadAsStringAsync().Result);
                Console.WriteLine($"\n{vehicle}\n");
            }
            else
            {
                Console.WriteLine($"\nStatus Code: {response.StatusCode}\n");
            }
        }

        private static void DeleteVehicle()
        {
            var id = GetInputVehicleId();

            var response = DeleteContent(id);

            Console.WriteLine($"\nStatus Code: {response.StatusCode}\n");
        }

        private static void DisplayAllTransactions()
        {
            string response = GetResponse($"https://{host}/api/transactions/all");
            if (string.IsNullOrEmpty(response))
            {
                return;
            }

            Console.WriteLine("\nAll transactions:\n");

            Console.WriteLine(response);
        }

        private static void DisplayLastTransactions()
        {
            string response = GetResponse($"https://{host}/api/transactions/last");
            if (string.IsNullOrEmpty(response))
            {
                return;
            }

            Console.WriteLine("\nLast transactions:\n");

            foreach (var transaction in JsonConvert.DeserializeObject<List<TransactionInfo>>(response))
            {
                Console.WriteLine($"{transaction}\n");
            }
        }

        private static void TopUpVehicle()
        {
            var peyment = GetInputPayment();

            var content = new StringContent(JsonConvert.SerializeObject(peyment), Encoding.UTF8, "application/json");
            var response = PutContent(content);

            if (response != null && response.IsSuccessStatusCode)
            {
                var vehicle = JsonConvert.DeserializeObject<Vehicle>(response.Content.ReadAsStringAsync().Result);
                Console.WriteLine($"\n{vehicle}\n");
            }
            else
            {
                Console.WriteLine($"\nStatus Code: {response.StatusCode}\n");
            }
        }

        private static string GetInputVehicleId()
        {
            Console.Write("ID (format: \"XX-YYYY-XX\", where X is any uppercase letter and Y is any digit): ");
            return Console.ReadLine();
        }

        private static Vehicle GetInputVehicle()
        {
            Console.WriteLine("\nEnter information about transport:");
            var vehicle = new Vehicle();

            vehicle.Id = GetInputVehicleId();

            DisplayVehicleTypes();

            int vehicleType;
            Console.Write("Vehicle type (enter number that indicates type of vehicle suggested): ");
            while (!int.TryParse(Console.ReadLine(), out vehicleType))
            {
                Console.Write("\nInvalid vehicle type entered. Please try again: ");
            }
            vehicle.VehicleType = vehicleType;

            decimal balance;
            Console.Write("Balance: ");
            while (!decimal.TryParse(Console.ReadLine(), out balance))
            {
                Console.Write("\nInvalid balance entered. Please try again: ");
            }
            vehicle.Balance = balance;

            return vehicle;
        }

        private static Payment GetInputPayment()
        {
            Console.WriteLine("\nEnter information about payment:");
            var payment = new Payment();

            payment.Id = GetInputVehicleId();

            decimal sum;
            Console.Write("\nAmount: ");
            while (!decimal.TryParse(Console.ReadLine(), out sum))
            {
                Console.Write("\nInvalid balance entered. Please try again: ");
            }
            payment.Sum = sum;

            return payment;
        }

        private static void DisplayVehicleTypes()
        {
            var types = Enum.GetValues(typeof(VehicleType));
            for (int i = 0; i < types.Length; i++)
            {
                Console.WriteLine($"{i} - {types.GetValue(i)}");
            }
        }

        private static HttpResponseMessage DeleteContent(string id)
        {
            HttpResponseMessage response = null;
            try
            {
                response = client.DeleteAsync($"https://{host}/api/vehicles/{id}").Result;
            }
            catch (AggregateException)
            {
                Console.WriteLine($"\n{endpointException}");
                EnterHost();
            }
            return response;
        }

        private static HttpResponseMessage PostContent(StringContent content)
        {
            HttpResponseMessage response = null;
            try
            {
                response = client.PostAsync($"https://{host}/api/vehicles", content).Result;
            }
            catch (AggregateException)
            {
                Console.WriteLine($"\n{endpointException}");
                EnterHost();
            }
            return response;
        }


        private static HttpResponseMessage PutContent(StringContent content)
        {
            HttpResponseMessage response = null;
            try
            {
                response = client.PutAsync($"https://{host}/api/transactions/topUpVehicle", content).Result;
            }
            catch (AggregateException)
            {
                Console.WriteLine($"\n{endpointException}");
                EnterHost();
            }
            return response;
        }

        private static string GetResponse(string endpoint)
        {
            try
            {
                return client.GetStringAsync(endpoint).Result;
            }
            catch (AggregateException)
            {
                Console.WriteLine($"\n{endpointException}");
                EnterHost();
                return string.Empty;
            }
        }

        private static void CloseApp()
        {
            Environment.Exit(0);
        }
    }
}
