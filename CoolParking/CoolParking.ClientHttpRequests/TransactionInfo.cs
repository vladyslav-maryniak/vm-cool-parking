﻿using Newtonsoft.Json;
using System;

namespace CoolParking.ClientHttpRequests
{
    class TransactionInfo
    {
        [JsonProperty]
        public DateTime Time { get; set; }
        [JsonProperty]
        public string Id { get; set; }
        [JsonProperty]
        public decimal Sum { get; set; }

        public override string ToString()
        {
            return $"Time: {Time}\nID: {Id}\nSum: {Sum}";
        }
    }
}
