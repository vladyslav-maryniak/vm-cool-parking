﻿using Newtonsoft.Json;

namespace CoolParking.ClientHttpRequests
{
    public class Payment
    {
        [JsonProperty]
        public string Id { get; set; }
        [JsonProperty]
        public decimal Sum { get; set; }
    }
}
