﻿using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Services
{
    public class ParkingSessionService : IParkingSessionService
    {
        private IParkingService parkingService;

        public ParkingSessionService(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        public void DeleteVehicle(string id)
        {
            parkingService.RemoveVehicle(id);
        }

        public string GetAllTransactions()
        {
            return parkingService.ReadFromLog();
        }

        public decimal GetBalance()
        {
            return parkingService.GetBalance();
        }

        public int GetCapacity()
        {
            return parkingService.GetCapacity();
        }

        public int GetFreePlaces()
        {
            return parkingService.GetFreePlaces();
        }

        public TransactionInfo[] GetLastTransactions()
        {
            return parkingService.GetLastParkingTransactions();
        }

        public Vehicle GetVehicle(string id)
        {
            var vehicle = parkingService.GetVehicles().FirstOrDefault(x => x.Id == id) ??
                throw new ArgumentNullException("Vehicle with the specified ID is not parked");
            
            return vehicle;
        }

        public List<Vehicle> GetVehicles()
        {
            return new List<Vehicle>(parkingService.GetVehicles());
        }

        public Vehicle PostVehicle(VehicleData vehicleData)
        {
            var vehicle = new Vehicle(vehicleData.Id, (VehicleType)vehicleData.VehicleType, (decimal)vehicleData.Balance);
            parkingService.AddVehicle(vehicle);
            return vehicle;
        }

        public Vehicle TopUpVehicle(string id, decimal sum)
        {
            parkingService.TopUpVehicle(id, sum);
            return GetVehicle(id);
        }
    }
}
