﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingSessionService parkingSessionService;

        public TransactionsController(IParkingSessionService parkingSessionService)
        {
            this.parkingSessionService = parkingSessionService;
        }

        // GET api/transactions/last
        [HttpGet("[action]")]
        public ActionResult<TransactionInfo[]> Last()
        {
            return parkingSessionService.GetLastTransactions();
        }

        // GET api/transactions/all
        [HttpGet("[action]")]
        public ActionResult<string> All()
        {
            try
            {
                return parkingSessionService.GetAllTransactions();
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }

        // PUT api/transactions/topUpVehicle
        [HttpPut("[action]")]
        public ActionResult<Vehicle> TopUpVehicle(Payment payment)
        {
            if (!Vehicle.IsValidRegistrationPlateNumber(payment.Id) || (decimal)payment.Sum <= 0.0M)
            {
                return BadRequest();
            }

            try
            {
                return parkingSessionService.TopUpVehicle(payment.Id, (decimal)payment.Sum);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
        }
    }
}
