﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingSessionService parkingSessionService;

        public VehiclesController(IParkingSessionService parkingSessionService)
        {
            this.parkingSessionService = parkingSessionService;
        }

        // GET api/vehicles
        [HttpGet]
        public ActionResult<List<Vehicle>> GetVehicles()
        {
            return parkingSessionService.GetVehicles();
        }

        // GET api/vehicles/id
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            if (!Vehicle.IsValidRegistrationPlateNumber(id))
            {
                return BadRequest();
            }

            try
            {
                return parkingSessionService.GetVehicle(id);
            }
            catch (ArgumentNullException)
            {
                return NotFound();
            }
        }

        // POST api/vehicles
        [HttpPost]
        public ActionResult<Vehicle> PostVehicle(VehicleData vehicle)
        {
            try
            {
                var response = parkingSessionService.PostVehicle(vehicle);
                return CreatedAtAction(nameof(GetVehicle), new { id = vehicle.Id }, response);
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        // DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            if (!Vehicle.IsValidRegistrationPlateNumber(id))
            {
                return BadRequest();
            }
            
            try
            {
                parkingSessionService.DeleteVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch (InvalidOperationException)
            {
                return Conflict();
            }
        }
    }
}
