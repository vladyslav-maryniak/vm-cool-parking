﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IParkingSessionService
    {
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
        List<Vehicle> GetVehicles();
        Vehicle GetVehicle(string id);
        Vehicle PostVehicle(VehicleData vehicle);
        void DeleteVehicle(string id);
        TransactionInfo[] GetLastTransactions();
        string GetAllTransactions();
        Vehicle TopUpVehicle(string id, decimal sum);
    }
}
